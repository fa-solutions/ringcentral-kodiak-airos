
class RING_CENTRAL {
    constructor({ FAClient, RingCentral, serviceName, callType, smsType, allCallView, allSmsView } = {}) {
        this.FAClient = FAClient;
        this.currentMatchedContact = null;
        this.RingCentral = RingCentral;
        this.SERVICE_NAME = serviceName || "FreeAgent";
        this.callType = callType;
        this.smsType = smsType;
        this.allCallView = allCallView;
        this.allSmsView = allSmsView;
        this.ongoingCall;
        this.matchingContacts;
        this.currentCallNumber;
        this.ringCentralListener = this.ringCentralListener;
        this.eventData;
        this.agentId;

        this.FAClient.on("onCall", ({record, agentId}) => {
            this.eventData = record;
            this.currentMatchedContact = record;
            this.agentId = agentId;
            let phone = this.eventData.field_values.work_phone.value;
            if(phone) {
                this.makeCall(phone);
            }
        });

        this.FAClient.on("onSMS", ({record, agentId}) => {
            this.eventData = record;
            this.currentMatchedContact = record;
            this.agentId = agentId;
            let phone = this.eventData.field_values.work_phone.value;
            if(phone) {
                this.sendSMS(phone);
            }
        });
    }

    ringCentralListener(event) {
        let data = event.data;
        let call = data.call;
        if (!data)
            return;
        switch (data.type) {
            case 'rc-call-ring-notify':
                this.setCurrentMatchedContact(call.from ,this.renderContactButtton.bind(this));
                this.ongoingCall = true;
                this.FAClient.open();
                break;
            case 'rc-call-init-notify':
                this.FAClient.open();
                this.ongoingCall = true;
                this.setCurrentMatchedContact(call.to ,this.renderContactButtton.bind(this));
                break;
            case 'rc-call-end-notify':
                this.matchingContacts = false;
                this.currentCallNumber = null;
                this.ongoingCall = false;
                this.handleCallEnd(data);
                cleanFooter();
                break;
            case 'rc-message-updated-notify':
                this.handleUpdatedMessage(data);
                break;
            case 'rc-inbound-message-notify':
                const phoneNumber = _.get(data, 'message.from.phoneNumber');
                this.RingCentral.postMessage({
                    type: 'rc-adapter-new-sms',
                    phoneNumber,
                    conversation: true,
                }, '*');
                this.FAClient.open();
                break;
            case 'rc-route-changed-notify':
                if (!data.path.includes('/calls/active')) {
                    cleanFooter();
                }
                if (data.path === '/history') {
                    const footerButton = document.createElement('button');
                    footerButton.innerText = 'All Phone Calls';
                    footerButton.onclick = () => {
                        this.FAClient.navigateTo(this.allCallView);
                    };
                    footer.appendChild(footerButton);
                } else if (data.path === '/messages') {
                    const footerButton = document.createElement('button');
                    footerButton.innerText = 'All SMS';
                    footerButton.onclick = () => {
                        this.FAClient.navigateTo(this.allSmsView);
                    };
                    footer.appendChild(footerButton);
                }
                break;
            case 'rc-login-status-notify':
                if (data.loggedIn) {
                    this.RingCentral.postMessage(
                        {
                            type: 'rc-adapter-register-third-party-service',
                            service: {
                                name: this.SERVICE_NAME,
                                callLoggerPath: '/callLogger',
                                callLoggerTitle: `Log to ${this.SERVICE_NAME}`,
                                messageLoggerPath: '/messageLogger',
                                messageLoggerTitle: `Log to ${this.SERVICE_NAME}`,
                                contactMatchPath: '/contacts/match',
                                contactsPath: '/contacts',
                                showLogModal: true,
                            },
                        }, '*');
                }
                break;
            case 'rc-post-message-request':
                if (data.path === '/callLogger') {
                    this.logCallsManually(data);
                }
                if (data.path === '/messageLogger') {
                    this.logMessagesManually(data,() => {});
                }
                if (data.path === '/contacts/match') {
                    this.matchContacts(data);
                }
                break;
            case 'rc-callLogger-auto-log-notify':
                break;
            default:
                break;
        }
    }

    makeCall(phoneNumber) {
        this.RingCentral.postMessage(
            {
                type: 'rc-adapter-new-call',
                phoneNumber,
                toCall: true,
            }, '*');
    }
    sendSMS(phoneNumber) {
        this.RingCentral.postMessage(
            {
                type: 'rc-adapter-new-sms',
                phoneNumber
            }, '*');
    }

    setCurrentMatchedContact(from, callback) {
        if (!this.eventData || Object.keys(this.eventData).length === 0) {
            this.getCurrentMatchedContactDetails(from, callback);
        } else {
            callback();
        }
    }

    getCurrentMatchedContactDetails(fromPhoneNumber, callback) {
        let variables = {
            entity: "contact",
            filters: [
                {
                    field_name: 'work_phone',
                    operator: 'contains',
                    values: [fromPhoneNumber],
                },
            ],
        };
        this.FAClient.listEntityValues(variables, (contacts) => {
                this.currentMatchedContact = contacts.length > 0 ? contacts[0] : null;
                callback();
            }
        );
    }

    handleCallEnd(data) {
        const callId = _.get(data, 'call.callId');
        const callValues = this.getCallValuesFromData(data);
        this.logCallById(callId, callValues, ({ entity_value: phoneCall }) => {
            let values = JSON.parse(JSON.stringify(phoneCall));
            this.FAClient.showModal('entityFormModal', {
                entity: phoneAppletPrefixName,
                entityInstance : values
            });
        });
    }

    handleUpdatedMessage(data) {
        const phoneNumber = _.get(data, 'message.from.phoneNumber');
        const message = _.get(data, 'message');
        const smsValues = this.getSMSValues(message);

        this.logMessages([message], [phoneNumber], () => {});

     }
    logCallsManually(data) {
        const responseId = data.requestId;
        const triggerType = data.body && data.body.triggerType;

        if (triggerType) {
            return this.RingCentral.postMessage(
            {
                type: 'rc-post-message-response',
                responseId,
                response: { data: 'ok' },
            },
            '*',
            );
        }

        const callId = _.get(data, 'body.call.id');
        const callValues = this.getCallValuesFromData(data);

        this.logCallById(callId, callValues, () => {
            this.RingCentral.postMessage(
            {
                type: 'rc-post-message-response',
                responseId,
                response: { data: 'ok' },
            },
            '*',
            );
        });
     }
    logMessagesManually(data) {
        const triggerType = _.get(data, 'body.triggerType');

        if (triggerType !== 'manual') {
          return this.RingCentral.postMessage(
            {
              type: 'rc-post-message-response',
              responseId: data.requestId,
              response: { data: 'ok' },
            },
            '*',
          );
        }

        const conversation = _.get(data, 'body.conversation');
        const messages = _.get(conversation, 'messages', []);
        const phoneNumbers = _.get(conversation, 'correspondents', [])
              .map((c) => c.phoneNumber);

        this.logMessages(messages, phoneNumbers, () => {
          this.RingCentral.postMessage(
            {
              type: 'rc-post-message-response',
              responseId: data.requestId,
              response: { data: 'ok' },
            },
            '*',
          );
        })
     }
     setMatchedContacts(contact, data, phoneNumbers){
        console.log({contact})
        let matchedContacts = {};
        if (contact) {
          matchedContacts = phoneNumbers.reduce((acc, phoneNumber) => {
            return {
              ...acc,
              [phoneNumber]: [
                {
                  id: contact.id,
                  type: this.SERVICE_NAME,
                  name: _.get(contact, 'field_values.full_name.value'),
                  phoneNumbers: PHONE_FIELD_TYPES
                    .map((phoneField) => ({
                      phoneNumber: _.get(
                        contact,
                        `field_values[${phoneField.field}].value`,
                      ),
                      phoneType: phoneField.type,
                    }))
                    .filter((f) => f.phoneNumber),
                },
              ],
            };
          }, {});
        }
        this.renderContactButtton();
        this.RingCentral.postMessage(
          {
            type: 'rc-post-message-response',
            responseId: data.requestId,
            response: {
              data: matchedContacts,
            },
          },
          '*',
        );

     }
     parseNumbersForPattern(numbers) {
        return numbers.reduce((prev, next) => {
            const parsedNumber = libphonenumber.parsePhoneNumberFromString(next);
            return [
                ...prev,
                ...(parsedNumber
                    ? [parsedNumber.number, parsedNumber.nationalNumber]
                    : []),
            ];
        }, [])
            .filter((n) => n && n.length > 4).join('|');
    }
    matchContacts(data) {
        if (!this.ongoingCall) return;

        const phoneNumbers = data.body.phoneNumbers;


        if (this.currentMatchedContact) {
            return this.setMatchedContacts(this.currentMatchedContact, data, phoneNumbers);
        }

        console.log({phoneNumbers})
        const pattern = this.parseNumbersForPattern(phoneNumbers);

        //if (phoneNumbers.length > 1) return;
        console.log({pattern})
        this.FAClient.listEntityValues(
            {
                entity: 'contact',
                pattern,
                limit: 1,
            },
            (contacts) => {
                this.currentMatchedContact = _.get(contacts, '[0]');
                this.setMatchedContacts(_.get(contacts, '[0]'), data, phoneNumbers);
            },
        );
    }
    logMessages(messages,phoneNumbers,callback){
        const messagesIDs = messages.map((m) => `${m.id}`);

        this.FAClient.listEntityValues(
          {
            entity: phoneAppletPrefixName,
            filters: [
              {
                field_name: phoneAppletFields.ringcentralId,
                operator: 'includes',
                values: messagesIDs,
              },
            ],
          },
          (existingMessages) => {
            const newMessages = _.differenceWith(
              messages,
              existingMessages,
              (a, b) =>
                `${a.id}` ===
                _.get(
                  b,
                  `field_values.${phoneAppletFields.ringcentralId}.value`,
                ),
            );

            const pattern = phoneNumbers;

            this.FAClient.listEntityValues(
              {
                entity: 'contact',
                pattern,
                limit: 1,
              },
              (contacts) => {
                const contact = _.get(contacts, '[0]');
                newMessages.map((message, index) => {
                  const isLast = index === newMessages.length - 1;
                  const smsValues = this.getSMSValues(message, contact);
                  this.FAClient.createEntity(smsValues, (created) => {
                    if (isLast) {
                      callback();
                    }
                  });
                });
              },
            );
          }
        );
    }
    getSMSValues(message,contact) {
        const ringcentralID = _.get(message, 'id');
        let from = _.get(message, 'from.phoneNumber');
        let to = _.get(message, 'to[0].phoneNumber');
        const subject = _.get(message, 'subject');
        const direction = _.get(message, 'direction');
        const messageStatus = _.get(message, 'messageStatus');
        const readStatus = _.get(message, 'readStatus');

        from = from ? from.replace("*","x") : null;
        to = to ? to.replace("*","x") : null;

        return {
            entity: phoneAppletPrefixName,
            field_values: {
            [phoneAppletFields.from]: from,
            [phoneAppletFields.to]: to,
            [phoneAppletFields.contact]: contact && contact.id,
            [phoneAppletFields.subject]: subject,
            [phoneAppletFields.type]: this.smsType,
            [phoneAppletFields.messageStatus]: messageStatus,
            [phoneAppletFields.readStatus]: readStatus,
            [phoneAppletFields.direction]: direction,
            [phoneAppletFields.ringcentralId]: `${ringcentralID}`,
            [phoneAppletFields.owner]: this.agentId || ''
            },
        };
    }

    getCallValuesFromData(data) {
        const callId = _.get(data, 'body.call.id', _.get(data, 'call.callId'));
        let from = _.get(data, 'body.call.from.phoneNumber', _.get(data, 'call.fromNumber') || _.get(data, 'call.from'));
        let to = _.get(data, 'body.call.to.phoneNumber', _.get(data, 'call.to'));
        const direction = _.get(data, 'body.call.direction', _.get(data, 'call.direction'));
        const note = _.get(data, 'body.note', '');
        const contact = _.get(this.currentMatchedContact, 'id');
        const duration = _.get(data, 'body.call.duration', Math.round((_.get(data, 'call.endTime', 0) - _.get(data, 'call.creationTime', 0)) / 1000));

        from = from ? from.replace("*","x") : null;
        to = to ? to.replace("*","x") : null;

        return {
            entity: phoneAppletPrefixName,
            field_values: {
                [phoneAppletFields.from]: from,
                [phoneAppletFields.to]: to,
                [phoneAppletFields.contact]: contact,
                [phoneAppletFields.duration]: duration,
                [phoneAppletFields.direction]: direction,
                [phoneAppletFields.ringcentralId]: callId,
                [phoneAppletFields.note]: note,
                [phoneAppletFields.type] : this.callType,
                [phoneAppletFields.owner] : this.agentId || ''
            },
        };
    }

    renderContactButtton() {
        cleanFooter();
        let url = "/entity/contact/view/all/create";
        let buttonValue = "Create New Contact";
        const footer = document.getElementById('footer');

        if (this.currentMatchedContact){
            buttonValue = `Navigate to ${_.get(this.currentMatchedContact,'field_values.full_name.value', '')}`;
            url = `/contact/view/${this.currentMatchedContact.id}`
        }

        const footerButton = document.createElement('button');
        footerButton.innerText = buttonValue;

        footerButton.onclick = () => {
            this.FAClient.navigateTo(url);
        };

        footer.appendChild(footerButton);
    }

    logCallById(callId, callValues, callback) {
        this.FAClient.listEntityValues(
            {
                entity: phoneAppletPrefixName,
                filters: [
                    {
                        field_name: phoneAppletFields.ringcentralId,
                        operator: 'includes',
                        values: [callId],
                    },
                ],
            },
            (existingPhoneCalls) => {
                const existingPhoneCall = _.get(existingPhoneCalls, '[0]');
                this.FAClient.upsertEntity({
                    id: _.get(existingPhoneCall, 'id', ''),
                    ...callValues,
                }, callback);
            }
        );
    }
}
