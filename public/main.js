
const APPLET_ID = "aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3JpbmdjZW50cmFsLWtvZGlhay1haXJvcw=="; // aHR0cDovL2xvY2FsaG9zdDo1MDAw

let eventData = {};

var ffAAppletClient = null;

startupService();

function startupService() {
    console.log(APPLET_ID)
    ffAAppletClient = new FAAppletClient({
        appletId: APPLET_ID
    });

    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    const { clientId, clientSecret, redirectUri, appServer, callType, smsType, allCallView, allSmsView, serviceName } = ffAAppletClient.params;
    if (clientId && clientSecret) {
        const iFrame = document.createElement('iframe');
        iFrame.src = `https://ringcentral.github.io/ringcentral-embeddable/app.html?clientId=${clientId}&clientSecret=${clientSecret}&appServer=${appServer}&redirectUri=${redirectUri}`;
        iFrame.style = 'width: 100%; height: 100%; border: none;';
        iFrame.allow = 'microphone';
        removeTextMessage();
        document.getElementById('frameContainer').appendChild(iFrame);

        const RING_CENTRAL_HANDLER = new RING_CENTRAL({
            RingCentral: iFrame.contentWindow,
            FAClient: ffAAppletClient,
            serviceName,
            callType,
            smsType,
            allCallView,
            allSmsView
        });

        window.addEventListener('message', RING_CENTRAL_HANDLER.ringCentralListener.bind(RING_CENTRAL_HANDLER));
        console.log(RING_CENTRAL_HANDLER);
    }
}

function removeTextMessage() {
    const loadingText = document.getElementById('loading-text');
    loadingText.remove();
}

function cleanFooter() {
    const footer = document.getElementById('footer');
    footer.innerHTML = '';
}

