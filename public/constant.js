/*

 ring central app params configuration example

{
    "clientId" : "ring central client id",
    "clientSecret": "ring central secret",
    "redirectUri": "https://ringcentral.github.io/ringcentral-embeddable/redirect.html",
    "appServer": "https://platform.devtest.ringcentral.com",
    "callType": "fa5d991b-afc9-4df1-ac6e-50fbedd67577",
    "smsType": "d823d53b-66e5-4815-8262-4ce7302323b6",
    "allCallView" : "/entity/ring_central/view/2f5722a0-e1b0-4dd0-ace0-8418d7b98291",
    "allSmsView": "/entity/ring_central/view/84faa801-da21-4727-a3ad-38a3069bac4f",
    "serviceName": "FreeAgent CRM App"
}


 */